// Get the form element
const form = document.querySelector('form');

// Add event listener for form submission
form.addEventListener('submit', function(event) {
  // Prevent the default form submission behavior
  event.preventDefault();

  // Perform form validation
  const nameInput = document.querySelector('#name');
  const emailInput = document.querySelector('#email');

  // Check if name is empty
  if (nameInput.value.trim() === '') {
    alert('Please enter your name');
    return;
  }

  // Check if email is empty or invalid
  if (emailInput.value.trim() === '') {
    alert('Please enter your email');
    return;
  }

  // Check if email is valid using a regular expression
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(emailInput.value)) {
    alert('Please enter a valid email');
    return;
  }

  // If all validations pass, submit the form
  form.submit();
});
